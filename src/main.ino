/*
 *      a
 *     ---
 *  f |   | b
 *    | g |
 *     ---
 *  e |   | c
 *    |   |
 *     ---   .
 *      d    p
 *
 *  Bpgfedcba
 */

// https://www.instructables.com/id/7-segment-display-clock-1/
// Pins 2-8 are connected to the 7 segments of the display, 9 to the points.

#define a 2
#define b 3
#define c 4
#define d 5
#define e 6
#define f 7
#define g 8
#define p 9

#define D1 13
#define D2 12
#define D3 11
#define D4 10

#define N0 B00111111 // 0
#define N1 B00000110 // 1
#define N2 B01011011 // 2
#define N3 B01001111 // 3
#define N4 B01100110 // 4
#define N5 B01101101 // 5
#define N6 B01111101 // 6
#define N7 B00000111 // 7
#define N8 B01111111 // 8
#define N9 B01101111 // 9
#define PT B10000000 // .

static int temp = 0;

static bool counted = false;

// switch displays on/off
void display(byte num) {
    digitalWrite(D1, ~num >> 3 & 1);
    digitalWrite(D2, ~num >> 2 & 1);
    digitalWrite(D3, ~num >> 1 & 1);
    digitalWrite(D4, ~num >> 0 & 1);
}

// show a number on the display
void number(byte num) {
    digitalWrite(a, num >> 0 & 1);
    digitalWrite(b, num >> 1 & 1);
    digitalWrite(c, num >> 2 & 1);
    digitalWrite(d, num >> 3 & 1);
    digitalWrite(e, num >> 4 & 1);
    digitalWrite(f, num >> 5 & 1);
    digitalWrite(g, num >> 6 & 1);
    digitalWrite(p, num >> 7 & 1);
}

// it's the final countdown
void counter() {
    display(B0001);
    number(N9);
    delay(1000);
    number(N8);
    delay(1000);
    number(N7);
    delay(1000);
    number(N6);
    delay(1000);
    number(N5);
    delay(1000);
    number(N4);
    delay(1000);
    number(N3);
    delay(1000);
    number(N2);
    delay(1000);
    number(N1);
    delay(1000);
    number(N0);
    delay(1000);
}

void setup() {
    pinMode(a,  OUTPUT);
    pinMode(b,  OUTPUT);
    pinMode(c,  OUTPUT);
    pinMode(d,  OUTPUT);
    pinMode(e,  OUTPUT);
    pinMode(f,  OUTPUT);
    pinMode(g,  OUTPUT);
    pinMode(p,  OUTPUT);
    pinMode(D1, OUTPUT);
    pinMode(D2, OUTPUT);
    pinMode(D3, OUTPUT);
    pinMode(D4, OUTPUT);
}

void loop() {
    temp = analogRead(A1);
    Serial.println(temp);
    if(temp > 6) {
        if(!counted) {
            counter();
            counted = true;
        }
    }
    if(counted) {
        display(B1000);
        number(B01110110);
        delay(3);

        display(B0100);
        number(B01111001);
        delay(3);

        display(B0010);
        number(B00111000);
        delay(3);

        display(B0001);
        number(B01110011);
        delay(3);
    }
}

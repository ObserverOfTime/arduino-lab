PIOFLAGS =

.DEFAULT: upload

upload: ; platformio -f -c vim run -t upload $(PIOFLAGS)

clean: ; platformio -f -c vim run -t clean $(PIOFLAGS)

program: ; platformio -f -c vim run -t program $(PIOFLAGS)

uploadfs: ; platformio -f -c vim run -t uploadfs $(PIOFLAGS)

update: ; platformio -f -c vim update $(PIOFLAGS)

monitor: ; platformio -f -c vim device monitor $(PIOFLAGS)

%: ; platformio -f -c vim $@ $(PIOFLAGS)
